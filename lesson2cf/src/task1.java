import java.util.Scanner;
/*Напишите приложение которые инициализует значения массива размера 10 используя консольный ввод.
После чего выводит четыре строки содержащие
•    Все элементы по четным индексам.
•    Все четные элементы.
•    Все элементы в обратном порядке.
•    Только первый и последний элементы.
*/
public class task1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            System.out.printf("%d element of array: ", i);
            array[i] = input.nextInt();
        }
        input.close();

        evenIndexElements(array);
        everyEvenElement(array);
        allElementsInReverseOrder(array);
        firstAndLastElement(array);
    }

    public static void evenIndexElements(int[] array) {
        for(int i = 0; i < array.length; i++){
            if(array[i] % 2 == 0)
                System.out.print(array[i]);
        }

    }

    public static void everyEvenElement(int[] array) {
        for(int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0 && array[i] !=0)
                System.out.print(array[i]);
        }
    }

    public static void allElementsInReverseOrder(int[] array) {
        for(int i = array.length - 1; i >= 0; i--){
            System.out.print(array[i]);
        }
    }

    public static void firstAndLastElement(int[] array) {
        System.out.print(array[0]);
        System.out.print(array[array.length - 1]);
    }
}