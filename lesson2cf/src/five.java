import java.util.Scanner;
/*Год в котором 366 дней называется високосным годом.
Обычно годы которые делятся на 4 являются високосными годами, напримеи 1996.
Но годы которые делятся на 100 (например 1900) високосными не являются
но годы которые делятся на 400 являются високосными (например 2000).
Напишите приложение которое запрашивает у пользователя год и вычисляет
является ли этот год високосным или нет.*/
public class five {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Enter year:");
        int year = in.nextInt();

        if (year % 4 == 0 && year % 100 != 0 || year % 400 ==0) {

            System.out.println("Visokosniy");
        }
        else{
            System.out.println("Not visokosniy");
        }
    }
}
