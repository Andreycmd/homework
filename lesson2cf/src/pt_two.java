import java.util.Scanner;
/*
Напишите приложение которое запрашивает у пользователя два значения integer и выводит
 •    сумму
 •    разницу
 •    произведение
 •    среднее значение
 •    расстояние (абсолютное значение разницы)
 •    максимум
 •    минимум
 Подсказка: Функции max и min объявлены в классе Math.
*/
public class pt_two {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Enter first num:");
        int first = in.nextInt();
        Integer a = first;


        System.out.println("Enter second num:");
        int second = in.nextInt();
        Integer b = second;

        int result1 = a + b;
        int result2 = a - b;
        int result3 = a * b;
        int result4 = (a + b) / 2;
        System.out.println("Summa = " + result1);
        System.out.println("Raznost = " + result2);
        System.out.println("Proizvedenie = " + result3);
        System.out.println("Srednee = " + result4);
        System.out.println("Rasstoyanie = " + Math.abs(result2));
        System.out.println("Max = " + Math.max(a, b));
        System.out.println("Min = " + Math.min(a, b));

    }
}

