/*Напишите метод который переставлят элементы массива в обратном порядке.
Например если начальный массив был
1  4  9  16  9  7  4  9  11
он будет заменен на
11  9  4  7  9  16  9  4  1*/
public class task7 {
    public static void main(String[] args) {
        int[] array = {1, 4, 9, 16, 9, 7, 4, 9, 11};
        for (int i = array.length - 1; i >= 0; i--) {
            System.out.print(array[i]);
        }
    }
}
