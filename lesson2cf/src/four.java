import java.util.Scanner;
/*Напишите приложение которое считывает три числа и выводит “возрастающий” если числа идут в строго возрастающем порядке,
 “убывающий” если числа идут в строго убывающем порядке, и “немонотонная последовательность” в противном случае.
*/
public class four {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Enter first num:");
        int first = in.nextInt();

        System.out.println("Enter second num:");
        int second = in.nextInt();

        System.out.println("Enter third num:");
        int third = in.nextInt();

        if (third > second && second > first) {

            System.out.println("Your numerals increase");

        }
        else if (third < second && second < first) {

            System.out.println("Your numerals decrease");
        }
        else{
            System.out.println("Not monotonous");
        }
    }
}
