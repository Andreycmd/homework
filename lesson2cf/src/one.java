import java.util.Scanner;

/*Напишите приложение которое читает с консоли целое число и выводит информацию
о том является ли число отрицательным числом, нулем или положительным числом.
*/
public class one {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Enter your num:");
        int num = in.nextInt();

        if (num > 0) {

            System.out.println("Your numeral is positive");
        }

        else if (num < 0) {

            System.out.println("Your numeral is negative");
        }
         else{
            System.out.println("Your numeral is null");
        }
    }
}
