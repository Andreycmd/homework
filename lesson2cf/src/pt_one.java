import java.util.Scanner;
/*
Напишите приложение которое считывает число и выводит квадрат куб и четвертую степень.
Используйте метод Math.pow только для четвертой степени.
*/
public class pt_one {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Enter your num:");
        int num = in.nextInt();
        int a = 4;
        int result1 = num * num;
        int result2= num * num * num;
        System.out.println("Kvadrat = " + result1);
        System.out.println("Kub = " + result2);
        System.out.println("4-stepen " + Math.pow(num, a));

    }
}