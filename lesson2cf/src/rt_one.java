/*
Напишите класс Car. Добавьте поле для цвета машины и метод для покраски машины.
Напишите класс Shop. Добавьте метод для покраски машины. Напишите класс Owner
определите методы для задания и получения машины. Напишите программу которая
задаёт одну машину двум владельцам. Получите машину одного владельца и покрасьте
её используя класс Shop. Возьмите машину другого владельца и распечатайте ее цвет.
*/
public class rt_one {

    public static void main(String[] args) {

        Car Sedan = new Car("sedan" , "black");
        Owner Tom= new Owner("Tom", "sedan");
        Owner Bob= new Owner("Bob", "sedan");
        Shop Ncar= new Shop("sedan", "white");

        System.out.println("Car of Tom: name = " + Sedan.name + ", colour = "
                + Sedan.colour);

    }



    static class Car {

        Car(String name, String colour) {
            this.name = name;
            this.colour = colour;
        }

        public void ChangeColour(String ncolour) {
            colour = ncolour;
        }

        String colour;
        String name;
    }

    static class Owner {

        Owner(String name, String car) {
            this.name = name;
            this.car = car;
        }

        public void getcar(String ncar) {
            car = ncar;
        }

        String car;
        String name;
    }

    static class Shop {

        Shop(String car, String colour) {
            this.car = car;
            this.colour = colour;
        }

        public void ChangeColour(String ncolour) {
            colour = ncolour;
        }

        String colour;
        String car;
    }
}