import java.util.Scanner;
/*
Напишите приложение которое считывает с консоли число дней и выводит число миллисекунд в этом числе дней.
*/
public class pt_three {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Enter your days:");
        int days = in.nextInt();
        long a = days * 24 * 60 * 60 * 1000;
        System.out.println("In "+ days + " days - " + a + " ms");
    }
}