import java.util.Scanner;
/*
Напишите приложение которое считывает с консоли два логических значения и
выводит результат выполнения  AND, OR, XOR.
*/

public class pt_four {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Enter first:");
        boolean first = in.nextBoolean();

        System.out.println("Enter second:");
        boolean second = in.nextBoolean();
        boolean result1 = first & second;
        boolean result2 = first | second;
        boolean result3 = first ^ second;
        System.out.println("AND " + result1);
        System.out.println("OR = " + result2);
        System.out.println("XOR " + result3);
    }
}