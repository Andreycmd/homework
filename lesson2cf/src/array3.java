/*Вычислите переменную сумму всех элементов массива. Например если программа считывает
1  4  9  16  9  7  4  9  11
то результатом работы будет
1 – 4 + 9 – 16 + 9 – 7 + 4 – 9 + 11 = –2*/
public class array3 {

    public static void main(String[] args) {
        int x [] = {1 , 4 , 9 , 16 , 9 , 7 , 4 , 9 , 11};

        int sum = 0;

        for (int i = 0; i < x.length; i++) {
            if (i %2 != 0)
                x[i] = -x[i];
            sum = sum + x [i];
        }
        System.out.println(sum);
    }
}
