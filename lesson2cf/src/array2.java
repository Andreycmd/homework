/*Напишите метод sumWithoutSmallest который вычисляет сумму элементов массива кроме наименьшего используя один цикл.
 В цикле обновляйте сумму и наименьшее значение. После цикла верните разницу.*/
public class array2 {

    public static void main(String[] args) {
        int x [] = {5,3,7,12};

        int sum = 0;

        int x_min = x[0];

        for (int i = 0; i < x.length; i++) {
            if(x[i] < x_min)
                x_min = x[i];
            sum = sum + x [i];
        }
        sum = sum - x_min;
        System.out.println(sum);
    }
}

