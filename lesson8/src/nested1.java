/* Для стека создайте синхронизирующую обертку. Обертка должна быть статическим вложенным классом утилитного класса
   Stacks. Для получения обертки над стеком предусмотрите метод synchronizedStack приимающий Stack.*/
public class nested1 {


    public static Stack synchronizedStack(Stack stack) {
        return new SynchronizedStack(stack);
    }

    private static class SynchronizedStack implements Stack {

        private Stack stack;

        public SynchronizedStack(Stack stack) {
            this.stack = stack;
        }

        public int getValue() {
            return stack.getValue();
        }

        public synchronized int increment() {
            return stack.increment();
        }
    }
}

interface Stack {

    int getValue();
    int increment();
}

class SlowStack implements Stack{

    private int value;

    public int getValue(){
        return value;
    }

    public int increment(){
        return value++;
    }
}
