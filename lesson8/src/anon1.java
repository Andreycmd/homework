import java.util.Iterator;
/* Напишите приложение которое создает и запускает поток для вывода 10 приветствий за 10 секунд используя анонимный
класс реализующий Runnable. */
public class anon1 {

    public static void main(String[] args) {

        AnonymousStringList list = new AnonymousStringList();
        list.add("Hello");
        list.add("Hola");
        list.add("Aloha");
        list.add("Shalom");
        list.add("Buenas dias");
        list.add("Buon giorno");
        list.add("Guten Tag");
        list.add("Ola");
        list.add("Dobry den");
        list.add("Bonjour");

        Iterator<String> iter = list.iterator();

        while (iter.hasNext()) {
            System.out.println(iter.next());
        }
    }
}

class AnonymousStringList implements Iterable<String> {

    private String[] strings = new String[100];
    private int size;

    public void add(String string) {
        strings[size++] = string;
    }

    public Iterator<String> iterator() {
        return new Iterator<String>() {

            private int position = 0;

            public String next() {
                return strings[position++];
            }

            public boolean hasNext() {
                return position < size;
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }
}
