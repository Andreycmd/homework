public class inner {
/* Напишите класс Temperature для хранения информации о температуре воздуха по Цельсию. В классе есть методы для задания
и получения температуры. Кроме того в классе есть метод для получения адаптера который работает с температурой но по Фаренгейту.
Адаптер реализован как вложенный класс.*/
    public static void main(String[] args) {
        Termometer termo = new Termometer();

        termo.setTerm(50);
        termo.setTerm(100);
        System.out.println(termo);

        Termometer.TermometerFar far = termo.createTermometerFar();

        termo.setTerm(80);
        System.out.println("After setting to 80...");
        System.out.println(termo);

        System.out.println("On Farenheit");
        far.restoreState();
        System.out.println(termo);
    }
}
class Termometer {

    private int term;

    public Termometer() {
        this.term = 0;
    }

    public void setTerm(int term) {
        this.term = term;
    }

    public String toString() {
        return "Termometer [term=" + term + "]";
    }


    public TermometerFar createTermometerFar() {

        TermometerFar far = new TermometerFar();
        return far;
    }

    class TermometerFar {

        public int farterm;
        public TermometerFar() {
            int farterm = (term * 9 / 5) + 32;
        }


        public void restoreState() {
            int farterm = term ;
        }
    }
}
