/* Возможные состояния потока Java описываются перечислением Thread.State.
Различные состояния:TIMED_WAITING.
Напишите программу иллюстрирующую различные состояния потоков. Необходимо
чтобы все возможные состояния выводились. Для демонстрации различных состояний
могут использоваться различные потоки. Состояния потоков могут получаться как
внутри потока так и снаружи. */
public class thread3 {
    public static void main(String[]args) throws InterruptedException {
        DemoThread obj1 = new DemoThread();
        Thread t1 = new Thread(obj1);
        t1.start();
        Thread.sleep(1000);
        System.out.println(t1.getState());
    }
}

class DemoThread implements Runnable {
    @Override
    public void run() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            System.out.println("Thread interrupted");
        }
    }
}