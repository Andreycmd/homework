import sun.rmi.runtime.Log;
/* Возможные состояния потока Java описываются перечислением Thread.State.
Различные состояния: BLOCKED
Напишите программу иллюстрирующую различные состояния потоков. Необходимо
чтобы все возможные состояния выводились. Для демонстрации различных состояний
могут использоваться различные потоки. Состояния потоков могут получаться как
внутри потока так и снаружи. */
public class thread1 {
    public static void main(String[]args) throws InterruptedException {
        Thread t1 = new Thread(new DemoThreadB());
        Thread t2 = new Thread(new DemoThreadB());

        t1.start();
        t2.start();

        Thread.sleep(1000);

        System.out.println(t1.getName() + " state is - " + t1.getState());
        System.exit(0);
    }
}

class DemoThreadB implements Runnable {
    @Override
    public void run() {
        commonResource();
    }

    public static synchronized void commonResource() {
        while(true) {

        }
    }
}