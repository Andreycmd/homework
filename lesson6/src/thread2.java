
/* Возможные состояния потока Java описываются перечислением Thread.State.
Различные состояния:WAITING
Напишите программу иллюстрирующую различные состояния потоков. Необходимо
чтобы все возможные состояния выводились. Для демонстрации различных состояний
могут использоваться различные потоки. Состояния потоков могут получаться как
внутри потока так и снаружи. */
public class thread2 implements Runnable {
    public static Thread t1;

    public static void main(String[]args) {
        t1 = new Thread(new thread2());
        t1.start();
    }

    public void run() {
        Thread t2 = new Thread(new DemoThreadWS());
        t2.start();

        try {
            t2.join();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            System.out.println("Thread interrupted");
        }
    }
}

class DemoThreadWS implements Runnable {
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            System.out.println("Thread interrupted");
        }

        System.out.println(thread2.t1.getState());
    }
}