import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
/* Рассмотрим конвейер для производства лампочек. Каждую лампочку необходимо проверить и упаковать. Один поток
занимается проверкой лампочек (check). Другой поток занимается упаковкой лампочек (pack). Необходимо чтобы два потока
по очереди выводили в консоль слова check и pack. */
class Table {

    private boolean pinged = false;

    public synchronized void doCheck() throws InterruptedException {

        while (pinged == true) {
            wait();
        }
        System.out.println("-CHECK-");
        pinged = true;
        notify();
    }

    public synchronized void doPack() throws InterruptedException {
        while (pinged == false) {
            wait();
        }
        System.out.println("-PACK-");
        pinged = false;
        notify();
    }
}

class Check implements Runnable {

    private Table table;

    public Check(Table table) {
        this.table = table;
    }

    public void run() {
        try {
            while (!Thread.interrupted()) {
                table.doCheck();
            }
        } catch (InterruptedException e) {
            System.out.println("Exiting via interrupt");
        }
        System.out.println("Ending Check task");
    }
}

class Pack implements Runnable {

    private Table table;

    public Pack(Table table) {
        this.table = table;
    }

    public void run() {
        try {
            while (!Thread.interrupted()) {
                table.doPack();
            }
        } catch (InterruptedException e) {
            System.out.println("Exiting via interrupt");
        }
        System.out.println("Ending Pack task");
    }
}

class CheckPack {

    public static void main(String s[]) {

        Table table = new Table();
        Check ping = new Check(table);
        Pack pong = new Pack(table);

        ExecutorService exec = Executors.newCachedThreadPool();
        exec.execute(ping);
        exec.execute(pong);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        exec.shutdownNow();
    }
}