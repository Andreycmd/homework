public class thread implements Runnable {
    /* Возможные состояния потока Java описываются перечислением Thread.State.
    Различные состояния: NEW, RUNNABLE, TERMINATED.
    Напишите программу иллюстрирующую различные состояния потоков. Необходимо
    чтобы все возможные состояния выводились. Для демонстрации различных состояний
    могут использоваться различные потоки. Состояния потоков могут получаться как
    внутри потока так и снаружи. */
    Thread thread;

    public void run() {

        Thread.State state = Thread.currentThread().getState();
        String name = Thread.currentThread().getName();

        System.out.println(name + " state is - " + state);
    }

    public static void main(String[] args) throws InterruptedException {
        Thread th = new Thread(new thread());
        th.setName("My Thread");

        System.out.println(th.getName() + " state is - " + th.getState());
        th.start();
        Thread.sleep(1000);
        Thread.State state = th.getState();
        System.out.println(th.getName() + " state is - " + state);
    }
}
