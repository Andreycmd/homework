/* Объявите перечисление для отметки 5+. Должны быть константы для  A, B, C, D, E.
Разработайте тестовое приложение которое получает массив оценок и использует оператор
множественного выбора switch для комментария к значению оценки.*/
enum Grades {
    A, B, C, D, E
}

public class enum1 {

    public static void main(String[] args) {

        for (Grades grade : Grades.values()) {

            switch (grade) {
                case E:
                    System.out.println("E - awful.");
                    break;

                case D:
                    System.out.println("D - bad.");
                    break;

                case C:
                    System.out.println("C - not bad.");
                    break;
                case B:
                    System.out.println("B - good.");
                    break;
                case A:
                    System.out.println("A - great.");
                    break;

            }
        }
    }
}
