/* Объявите перечисление для банкнот РФ. Банкнота должна позволять получать номинал и
название города изображенного на банкноте. Для этого объявите конструктор и два поля с
номиналом банкноты и названием города. Разработайте тестовое приложение которое получает
массив банкнот и выводит номинал и название города для каждой банкноты. */

enum Money {
    Krasnoyarsk(10), Petersburg(50), Moscow(100), Arkhangelsk(500);
    private int value;

    private Money(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
};

class enum2 {

    public static void main(String[] args) {

        Money[] moneys = Money.values();

        for (Money money : moneys) {

            System.out.println("name = " + money.name() + ", ordinal = "
                    + money.ordinal() + ", value = " + money.getValue());
        }
    }
}
