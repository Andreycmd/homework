/*
Определите в классе Doctor метод introduce. Метод должен выводить информацию об имени и количестве статей.
Переопределите метод introduce в классе Professor. Метод должен выводить информацию об имени, количестве статей
и числе прочитанных курсов. Переопределите метод introduce в классе Dean. Метод должен выводить информацию об имени,
количестве статей, числе прочитанных курсов и числе преподавателей работающих на факультете.
*/

public class poly1 {

    public static void main(String[] args) {

        Dean dean = new Dean("Robert", 10, 1990, 5, 7);
        System.out.println(dean.getDescription());

        Prof prof = dean;
        System.out.println(prof.getDescription());


        Doc doc = prof;
        System.out.println(doc.getDescription());
    }
}
