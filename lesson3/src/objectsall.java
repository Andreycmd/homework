/*
Определите в классе Doctor методы equals и hashcode. Определите в классе Professor методы equals и hashcode используя
методы equals и hashcode для базового класса Doctor.  Напишите приложение демонстрирующее работу методов.
*/
import java.util.Date;
import java.util.GregorianCalendar;

public class objectsall{
    public static void main(String[] args) {

        System.out.println("Creating Employee object and initializing alice1 reference variable ...");
        Doctor alice1 = new Doctor("Alice Coder", 25000, 1987, 12, 15);
        System.out.println("alice1: " + alice1);

        System.out.println("Copying reference from alice1 to alice2 ...");
        Doctor alice2 = alice1;

        System.out.println("alice1 and alice2 reference the same object: " + (alice1 == alice2));
        System.out.println("alice1 and alice2 reference equal objects: " + alice1.equals(alice2) + "\n");

        System.out.println("Creating Employee object and initializing alice3 reference variable ...");
        Doctor alice3 = new Doctor("Alice Coder", 25000, 1987, 12, 15);
        System.out.println("alice1: " + alice3);

        System.out.println("alice1 and alice3 reference the same object: " + (alice1 == alice3));
        System.out.println("alice1 and alice3 reference equal objects: " + alice1.equals(alice3) + "\n");

        System.out.println("Creating Employee object and initializing bob reference variable ...");
        Doctor bob = new Doctor("Bob Proger", 25000, 1987, 12, 15);
        System.out.println("bob: " + bob);

        System.out.println("alice1 and bob reference the same object: " + (alice1 == bob));
        System.out.println("alice1 and bob reference equal objects: " + alice1.equals(bob));
    }
}

class Doctor implements Cloneable {
    public Doctor(String n, double s, int year, int month, int day) {
        name = n;
        states = s;
        GregorianCalendar calendar = new GregorianCalendar(year, month - 1, day);
        hireDay = calendar.getTime();
    }

    public String getName() {
        return name;
    }

    public double getStates() {
        return states;
    }

    public void setHireDay(int year, int month, int day) {
        Date newHireDay = new GregorianCalendar(year, month - 1, day).getTime();
        hireDay.setTime(newHireDay.getTime());
    }

    public Date getHireDay() {
        return hireDay;
    }

    public void raiseStates(double increase) {
        states += increase;
    }

    public boolean equals(Object otherObject) {

        if (this == otherObject)
            return true;

        if (otherObject == null)
            return false;

        if (getClass() != otherObject.getClass())
            return false;

        Doctor other = (Doctor) otherObject;

        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;


        return  states == other.states && hireDay.equals(other.hireDay);
    }

    public int hashCode() {
        return 7 * name.hashCode() + 11 * new Double(states).hashCode() + 13
                * hireDay.hashCode();
    }

    public String toString() {
        return getClass().getName() + "[name=" + name + ",states=" + states
                + ",hireDay=" + hireDay + "]";
    }

    public Doctor clone() throws CloneNotSupportedException {
        Doctor cloned = (Doctor) super.clone();
        cloned.hireDay = (Date) hireDay.clone();

        return cloned;
    }

    private String name;
    private double states;
    private Date hireDay;
}
class Professor extends Doctor {
    public Professor(String n, double s, int year, int month, int day) {
        super(n, s, year, month, day);
        courses = 0;
    }

    public double getStates() {
        double baseStates = super.getStates();
        return baseStates + courses;
    }

    public void setCourses(double b) {
        courses = b;
    }

    public boolean equals(Object otherObject) {
        if (!super.equals(otherObject))
            return false;
        Professor other = (Professor) otherObject;

        return courses == other.courses;
    }

    public int hashCode() {
        return super.hashCode() + 17 * new Double(courses).hashCode();
    }

    public String toString() {
        return super.toString() + "[courses=" + courses + "]";
    }

    private double courses;
}