/*
Определите в классе Doctor метод toString. Определите в классе Professor метод toString используя методы toString для
базового класса Doctor. Напишите приложение демонстрирующее работу метода.
*/
import java.util.Date;
import java.util.GregorianCalendar;

public class objectsall2{

    public static void main(String[] args) {

        Professor boss = new Professor("Carl Cracker", 30, 1987, 12, 15);
            boss.setCourses(50);

            Object[] objects  = new Object[3];

            objects [0] = boss;
            objects [1] = new Doctor("Harry Hacker", 25, 1989, 10, 1);
            objects [2] = new Doctor("Tommy Tester", 20, 1990, 3, 15);


            for (Object o : objects)
                System.out.println(o.toString());
    }
}

