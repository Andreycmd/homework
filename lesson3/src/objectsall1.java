/*
Определите в классе Doctor метод toString. Определите в классе Professor метод toString используя методы toString для
базового класса Doctor. Напишите приложение демонстрирующее работу метода.
*/
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

public class objectsall1{
    public static void main(String[] args) {

        System.out.println("Creating HashSet to store Employee objects ...");
        Set<Doctor> set = new HashSet<Doctor>();

        System.out.println("Creating two equal objects ...");
        Doctor one = new Doctor("Alice Coder", 25000, 1987, 12, 15);
        Doctor two = new Doctor("Alice Coder", 25000, 1987, 12, 15);

        System.out.println("Just to be absolutely sure let's test them for equality!");
        System.out.println("Objects are equal: " + one.equals(two));

        System.out.println("Adding one of them to HashSet ...");
        set.add(one);

        System.out.println("Hash set contains another one: " + set.contains(two));

        System.out.println("Should be true if hashCode is overriden correctly.");
        System.out.println("May be false if hashCode is not overriden!");
    }
}

