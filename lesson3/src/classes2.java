/*
Напишите класс MethodStudent c полями экземпляра для имени и для порядкового номера и
статическим полем для следующего свободного номера. Первый свободный номер должен
быть 1. Сделайте все поля приватными. Для задания имени создайте метод setName
для задания порядкового номера разработайте метод setStudentId.

Напишите приложение которое берет массив с именами студентов и создаёт объекты
MethodStudent присваивая им имена и порядковые номера и сохраняет ссылки на объекты в массив.
После чего приложение проходит по массиву объектов MethodStudent циклом foreach и
выводит для каждого имя и порядковый номер.
*/

public class classes2 {
    public static void main(String[] args) {

        String[] names = { "Harry Potter", "Ron Wesley", "Fred Wesley" };
        MethodStudent[] staff = new MethodStudent[3];

        for (int i = 0; i < 3; i++) {

            staff[i] = new MethodStudent();
            staff[i].setName(names[i]);
            staff[i].setId();
        }

        for (MethodStudent e : staff)
            System.out.println("name=" + e.getName() + ",id=" + e.getId());
    }
}

class MethodStudent {
    public void setName(String n) {
        name = n;
    }

    public String getName() {
        return name;
    }

    public void setId() {
        id = nextId;
        nextId++;
    }

    public int getId() {
        return id;
    }


    private int id;
    private String name;
    private static int nextId = 1;
}
