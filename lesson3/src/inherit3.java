/*
Создайте объект класса Dean. Продемонстрируйте работу методов. Проведите восходящее преобразование к типу Professor.
Продемонстрируйте работу доступных методов. Проверьте что тип объекта Dean. Проведите восходящее преобразование к типу
Doctor. Продемонстрируйте работу доступных методов. Проверьте что тип объекта Dean. Проведите нисходящее преобразование
к типу Professor. Продемонстрируйте работу доступных методов. Проведите нисходящее преобразование к типу Dean.
Продемонстрируйте работу доступных методов.
 */
public class inherit3 {
    public static void main(String[] args) {

            Doc doc = new Dean("Robert", 10 , 1990, 5, 7);

            System.out.println("A Doc: name = " + doc.getName() + ", states = "
                    + doc.getStates() + ", year = " + doc.getYear());

            System.out.println("\nI can cast to Prof: "
                    + (doc instanceof Prof));
            Prof prof = (Prof) doc;
            System.out.println("A Prof: name = " + prof.getName() + ", states = "
                    + prof.getStates() + ", year = " + prof.getYear() + ", courses = " + prof.getCourses());

            System.out.println("\nI can cast to Dean: "
                    + (prof instanceof Dean));
            Dean dean = (Dean) prof;
            System.out.println("A Dean: name = " + dean.getName() + ", states = "
                    + dean.getStates() + ", year = " + dean.getYear() + ", courses = " + dean.getCourses() + ", stuff = " + dean.getStuff());
        }
    }

