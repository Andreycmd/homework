/*
Напишите класс Doctor определите поля для имени, даты получения ученой степени и числа научных статей. Определите метод
для увеличения числа научных статей. Напишите класс Professor сделайте его наследником класса Doctor. Определите в нем
поле для числа прочитанных курсов. Напишите класс Dean сделайте его наследником класса Professor. Определите в нем поле
для числа профессоров работающих на факультете. Напишите приложение демонстрирующее работу методов.
*/

public class inherit1 {

    public static void main(String[] args) {

        Doc doc = new Doc();
        doc.setName("Robert");
        doc.setStates(10);
        doc.setYear(1990);
        System.out.println("A Doc: name = " + doc.getName() + ", states = "
                + doc.getStates() + ", year = " + doc.getYear());

        Prof prof = new Prof();
        prof.setName("Robert");
        prof.setStates(10);
        prof.setYear(1990);
        prof.setCourses(5);
        System.out.println("A Prof: name = " + prof.getName() + ", states = "
                + prof.getStates() + ", year = " + prof.getYear() + ", courses = " + prof.getCourses());

        Dean dean = new Dean();
        dean.setName("Robert");
        dean.setStates(10);
        dean.setYear(1990);
        dean.setCourses(5);
        dean.setStuff(7);
        System.out.println("A Dean: name = " + dean.getName() + ", states = "
                + dean.getStates() + ", year = " + dean.getYear() + ", courses = " + dean.getCourses() + ", stuff = " + dean.getStuff());
    }
}
class Doc {

    public Doc() {

    }

    public Doc(String name, int states, int year){
        this.name = name;
        this.states = states;
        this.year = year;
        System.out.println("Doc constructor is called setting name = " + name + "states = " + states + "and year = " +year);
    }
    public void setStates(int states) {
        this.states = states;
    }
    public int getStates() {
        return states;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public int getYear() {
        return year;
    }
    public void raiseStates(int increase) {
        states += increase;
    }
    public String getDescription() {
        return "A Doc: name = " + getName() + ", states = "
                + getStates() + ", year = " + getYear();
    }

    private String name;
    private int states;
    private int year;
}

class Prof extends Doc {

    public Prof() {
    }

    public Prof(String name, int states, int year, int courses) {
        super(name, states, year);
        this.courses = courses;
        System.out.println("Prof constructor is called setting courses = " + courses);
    }

    public double getCourses() {

        return courses;
    }

    public void setCourses(int courses) {
        this.courses = courses;
    }

    public String getDescription() {

        return "A Prof: name = " + getName() + ", states = "
                + getStates() + ", year = " + getYear() + ", courses = " + getCourses();
    }

   /*
   public String getDescription() {
      return super.getDescription() + "[salary = " + getSalary()+ "]";
   }
   */

    private int courses;
}
class Dean extends Prof {

    public Dean() {
    }

    public Dean(String name, int states, int year, int courses, int stuff) {
        super(name, states, year,courses);
        this.stuff = stuff;
        System.out.println("Dean constructor is called setting stuff = " + stuff);
    }

    public void setStuff(int stuff) {
        this.stuff = stuff;
    }

    public int getStuff() {
        return stuff;
    }


    public String getDescription(){
        return "A Dean: name = " + getName() + ", states = "
                + getStates() + ", year = " + getYear() + ", courses = " + getCourses() + ", stuff = " + getStuff();
    }


   /*
   public String getDescription() {
      return super.getDescription() + "[direct reports = " + getReports()+ "]";
   }
   */

    private int stuff;
}