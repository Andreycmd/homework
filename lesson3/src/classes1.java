/*
Напишите класс FieldStudent c полями экземпляра для имени и для порядкового номера и
статическим полем для следующего свободного номера. Первый свободный номер должен
быть 1. Напишите приложение которое берет массив с именами студентов и создаёт объекты
Student присваивая им имена и порядковые номера и сохраняет ссылки на объекты в массив.
После чего приложение проходит по массиву объектов Student циклом foreach и
выводит для каждого имя и порядковый номер.
*/
public class classes1 {
    public static void main(String[] args) {

        String[] names = { "Harry Potter", "Ron Wesley", "Fred Wesley" };
        FieldStudent[] staff = new FieldStudent[3];

        for (int i = 0; i < 3; i++) {

            staff[i] = new FieldStudent();
            staff[i].name = names[i];
            staff[i].id = FieldStudent.nextId++;
        }

        for (FieldStudent e : staff)
            System.out.println("name=" + e.name + ",id=" + e.id);
    }
}

class FieldStudent {

    public int id;
    public String name;
    public static int nextId = 1;
}
