
/*
Напишите класс Doctor определите поля для имени, даты получения ученой степени и числа научных статей. Определите метод
для увеличения числа научных статей. Напишите класс Professor сделайте его наследником класса Doctor. Определите в нем
поле для числа прочитанных курсов. Напишите приложение демонстрирующее работу методов.
*/
public class objects1 {

    public static void main(String[] args) {

        Doctor1 doc = new Doctor1();
        doc.setName("Robert");
        doc.setStates(20);
        doc.setYear(1990);
        System.out.println("A doctor: name = " + doc.getName()
                + ", states = " + doc.getStates() + ", date = "
                + doc.getYear());

        Professor1 pro = new Professor1();
        pro.setName("Robert");
        pro.setStates(50);
        pro.setYear(1999);
        pro.setCourses(15);
        System.out.println("A Professor: name = " + pro.getName()
                + ", states = " + pro.getStates() + ", date = "
                + pro.getYear() + ", courses = " + pro.getCourses());
    }
}

class Doctor1 {

    public Doctor1() {

    }
    public Doctor1(String name, int states, int year) {
        this.name = name;
        this.states = states;
        this.year= year;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public void setStates(int states) {
        this.states = states;
    }
    public int getStates() {
        return states;
    }
    public void setYear(int year) {
        this.year = year;
    }

    public int getYear() {
        return year;
    }
    public void raiseStates(int increase) {
        states += increase;
    }

    public int states;
    public String name;
    public int year;
}


class Professor1 extends Doctor1 {

    public int courses;

    public Professor1() {

        super();
    }

    public Professor1(String name, int states, int year) {
        super();


        this.courses = courses;
        System.out.println("Professor constructor is called setting courses = " + courses);
    }


    public double getCourses() {
        return courses;
    }

    public void setCourses(int courses) {
        this.courses = courses;
    }

}

