/*
Создайте объект класса Dean. Продемонстрируйте работу методов. Проведите восходящее преобразование к типу Professor.
Продемонстрируйте работу доступных методов. Проверьте что тип объекта Dean. Проведите восходящее преобразование к типу
Doctor. Продемонстрируйте работу доступных методов. Проверьте что тип объекта Dean. Проведите нисходящее преобразование
к типу Professor. Продемонстрируйте работу доступных методов. Проведите нисходящее преобразование к типу Dean.
Продемонстрируйте работу доступных методов.
 */
public class inherit2 {

    public static void main(String[] args) {

        Dean dean = new Dean();
        dean.setName("Robert");
        dean.setStates(10);
        dean.setYear(1990);
        dean.setCourses(5);
        dean.setStuff(7);
        System.out.println("A Dean: name = " + dean.getName() + ", states = "
                + dean.getStates() + ", year = " + dean.getYear() + ", courses = " + dean.getCourses() + ", stuff = " + dean.getStuff());

        Prof prof = dean;
        System.out.println("A Prof: name = " + prof.getName() + ", states = "
                + prof.getStates() + ", year = " + prof.getYear() + ", courses = " + prof.getCourses());
        System.out.println("Am I still a Dean: "
                + (prof instanceof Dean));


        Doc doc = prof;
        System.out.println("A Doc: name = " + doc.getName() + ", states = "
                + doc.getStates() + ", year = " + doc.getYear());
        System.out.println("Am I still a Dean: "
                + (doc instanceof Dean));

    }
}
