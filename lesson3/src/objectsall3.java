/*
Определите метод для клонирования объекта Doctor. Напишите приложение демонстрирующее работу метода.
*/
import java.util.Date;
import java.util.GregorianCalendar;

public class objectsall3{

    public static void main(String[] args) {
        try {
            Doctor original = new Doctor("Tonny Tester", 20, 2000, 10, 23);
            original.setHireDay(2000, 1, 1);
            Doctor copy = original.clone();

            System.out.println("original=" + original);
            System.out.println("copy=" + copy);

            System.out.println("changing copy now ...");

            copy.raiseStates(10);
            copy.setHireDay(2002, 12, 31);

            System.out.println("original=" + original);
            System.out.println("copy=" + copy);


        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}

