/*
Напишите класс ConstructorStudent c полями экземпляра для имени и для количества пройденных курсов.
Сделайте все поля приватными. Для задания имени и количества пройденных курсов создайте конструктор.

Напишите приложение которое создаёт объекты ConstructorStudent передавая в конструктор имена и
числа пройденных курсов. Затем приложение проходит по массиву объектов ConstructorStudent циклом foreach и
выводит для каждого имя и число пройденных курсов.
*/

public class classes4 {

    public static void main(String[] args) {

        ConstructorStudent[] staff = new ConstructorStudent[3];

        staff[0] = new ConstructorStudent("Harry Potter", 10);
        staff[1] = new ConstructorStudent("Ron Wesley", 8);
        staff[2] = new ConstructorStudent("Fred Wesley", 24);


        for (ConstructorStudent e : staff)
            System.out.println("name=" + e.getName() + ",courses=" + e.getCourses());
    }
}
class ConstructorStudent {

    public ConstructorStudent(String name, int courses) {
        this.name = name;
        this.courses = courses;
    }

    public String getName() {
        return name;
    }

    public int getCourses() {
        return courses;
    }


    private String name;
    private int courses;
}
