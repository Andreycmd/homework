/*
Напишите класс ManyConstructorStudent c полями экземпляра для имени, порядкового номера и среднего бала (double gpa).
Добавьте статическое поле для следующего свободного порядкового номера. Сделайте все поля приватными. Добавьте блок
инициализации экземпляра для задания порядкового номера студента.

Создайте три конструктора. Первый принимает имя и средний бал. Второй принимает только средний бал. Имя получается как
"Student #" + nextId. Третий конструктор не принимает параметров. Третий конструктор вызывает второй конструктор.
Второй конструктор вызывает первый конструктор.

Напишите приложение которое создаёт объекты MultipleConstructorStudent используя различные конструкторы.
Затем приложение проходит по массиву объектов MultipleConstructorStudent циклом foreach и выводит для каждого имя,
порядковый номер и средний бал.
*/

public class classes5 {
    public static void main(String[] args) {

        ManyConstructorStudent[] staff = new ManyConstructorStudent[3];

        staff[0] = new ManyConstructorStudent("Harry", 4.5);
        staff[1] = new ManyConstructorStudent(4.8);
        staff[2] = new ManyConstructorStudent();

        for (ManyConstructorStudent e : staff)
            System.out.println("name=" + e.getName() + ",id=" + e.getId()
                    + ",grade=" + e.getGrade());
    }
}
class ManyConstructorStudent {
    public ManyConstructorStudent(String n, double g) {
        name = n;
        grade = g;
    }

    public ManyConstructorStudent(double g) {
        this("Student #" + nextId, g);
    }

    public ManyConstructorStudent() {

    }

    public String getName() {
        return name;
    }

    public double getGrade() {
        return grade;
    }

    public int getId() {
        return id;
    }

    private static int nextId = 1;

    private int id;
    private String name = "";
    private double grade;

    {
        id = nextId;
        nextId++;
    }
}