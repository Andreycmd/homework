public abstract class abstract1 {

    public abstract1(int start, int finish) {

        this.start = start;
        this.finish = finish;
    }

    public int getStart() {

        return start;
    }

    public int getFinish() {

        return finish;
    }

    public boolean isConfirmed() {
        return on;
    }

    public void Reject() {
        on = false;
    }

    public void Apply() {
        on = true;
    }

    public abstract String getDescription();

    private int start;
    private int finish;
    private boolean on;
}

class SkypeAppointment extends abstract1 {

    public SkypeAppointment(int start, int finish) {
        super(start, finish);
    }

    public String getDescription() {
        return "SkypeAppointment";
    }

}

class PhoneAppointment extends abstract1{

    public PhoneAppointment(int start, int finish) {
        super(start, finish);
    }

    public String getDescription() {
        return "PhoneAppointment";
    }

}

class WhatsupAppointment extends abstract1{

    public WhatsupAppointment(int start, int finish) {
        super(start, finish);
    }

    public String getDescription() {
        return "WhatsupAppointment";
    }


}


