/*
Продемонстрируйте полиморфизм. Создайте массив ссылок типа Doctor. Заполните массив ссылками на объекты классов
Doctor, Professor, Dean. В цикле for each для каждого объекта вызовите метод introduce и выведите результат.
*/

public class poly2 {

    public static void main(String[] args) {

        Doc[] people = new Doc[3];

        people[0] = new Doc("John Whatson" , 10, 1990);
        people[1] = new Prof("Harry Hacker", 15, 1988,5);
        people[2] = new Dean("Carl Cracker", 25, 1985,10,5);

        DescriptionPrinter.printDescriptions(people);
    }
}
class DescriptionPrinter {

    public static void printDescriptions(Doc[] people) {

        for (Doc person : people) {
            System.out.println(person.getDescription());
        }
    }
}
