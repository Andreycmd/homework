/*
Напишите класс InitStudent c полями экземпляра для имени и для порядкового номера и
статическим полем для следующего свободного номера. Сделайте все поля приватными. Для задания имени создайте метод setName.
Создайте два блока инициализации статический и экземпляра. Статический блок инициализации должен инициализировать статическое
поле случайным значением от 0 до 10000. Блок инициализации экземпляра должен инициализировать поле для порядкого номера
значением следующего свободного номера.

      Random generator = new Random();
      nextId = generator.nextInt(10000);

Напишите приложение которое берет массив с именами студентов и создаёт объекты
InitStudent присваивая им имена и сохраняет ссылки на объекты в массив.
После чего приложение проходит по массиву объектов InitStudent циклом foreach и
выводит для каждого имя и порядковый номер.
*/

import java.util.Random;

public class classes3 {
    public static void main(String[] args) {

        String[] names = { "Harry Potter", "Ron Wesley", "Fred Wesley" };
        InitStudent[] staff = new InitStudent[3];

        for (int i = 0; i < 3; i++) {

            staff[i] = new InitStudent();
            staff[i].setName(names[i]);
        }

        for (InitStudent  e : staff)
            System.out.println("name=" + e.getName() + ",id=" + e.getId());
    }
}

class InitStudent {
    public void setName(String n) {
        name = n;
    }

    public String getName() {
        return name;
    }



    public int getId() {
        return id;
    }


    private int id;
    private String name;
    private static int nextId = 1;

    static {

        Random generator = new Random();
        nextId = generator.nextInt(10000);
        }

        {
        id = nextId;
        nextId++;
        }
}