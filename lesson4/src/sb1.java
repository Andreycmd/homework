/*
Разработайте метод который позволяет определить что строка является палиндромом.
Используйте метод reverse из класса StringBuilder.
*/
public class sb1 {

    public static void main(String[] args) {

        String palindrome = "dot saw i was tod";
        StringBuffer reverse = new StringBuffer(palindrome);
        reverse.reverse();
        if (palindrome.contentEquals(reverse)) {
            System.out.println("True");
        }
        else{
            System.out.println("False");
        }
    }
}
