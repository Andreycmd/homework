import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class re3 {

    public static void main(String[] args) {

        String text = "4444 3333 4444 3333 123 123 123 123";
        String patternString = "^([0-9][0-9][0-9][0-9] [0-9][0-9][0-9][0-9] [0-9][0-9][0-9][0-9] [0-9][0-9][0-9][0-9])$";

        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(text);

        String replaced = matcher.replaceAll("*");
        System.out.println("Initial text: " + text);
        System.out.println("Text after replace: " + replaced);
    }
}
