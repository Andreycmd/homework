/*
Разработайте метод который позволяет провести сортировку массива строк
по длине.
*/

import java.util.Arrays;
import java.util.Comparator;

public class string3 {
    public static void main(String[] args) {
    String [] words = {"apple", "banana", "blueberry", "kiwi"};
        Comparator<String> stringLengthComparator = new StringLengthSort();
        for (String str : words) {
            System.out.println(str);
        }
        Arrays.sort(words, stringLengthComparator);
        System.out.println("Sorted");
        for(String str : words){
            System.out.println(str);
        }
    }

    static class StringLengthSort implements Comparator<String>{
        public int compare (String o1, String o2){
            if (o1.length() > o2.length()){
                return 1;
            }
            else {
                if(o1.length() < o2.length()){
                    return -1;
                }
                else {
                    return 0;
                }
            }
        }
    }


}


