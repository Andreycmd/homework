/*
Разработайте метод для превращения строки содержащей десятизначный номер телефона
(такой как "4155551212" ) в более читаемую строку со скобками и дефисами как например "(415) 555-1212".
*/

public class sb3 {

    public static void main(String args[]) {

        StringBuffer sb = new StringBuffer("4155551212");
        System.out.println("Before insert: " + sb);

        sb.insert(0, "(");
        sb.insert(4, ") ");
        sb.insert(9, "-");
        System.out.println("After insert: " + sb);
    }
}

