/*
Разработайте метод который позволяет определить что строка является палиндромом.
Используйте подход meet-in-the-middle.
*/
public class string1 {

    public static void main(String[] args) {

        String palindrome = "dot saw i was tod";
        int len = palindrome.length();
        char[] tempCharArray = new char[len];
        char[] charArray = new char[len];

        for (int i = 0; i < len; i++) {
            tempCharArray[i] = palindrome.charAt(i);
        }

        for (int j = 0; j < len; j++) {
            charArray[j] = tempCharArray[len - 1 - j];
        }
        String reversePalindrome = new String(charArray);
        if (palindrome.equals(reversePalindrome)) {
            System.out.println("True");
        }
        else{
            System.out.println("False");
        }
    }
}
