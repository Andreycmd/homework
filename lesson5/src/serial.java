import java.io.*;

/* Создайте классы для человека и для собаки. Напишите программу которая создает человека и собаку.
Задайте человеку ссылку на объект собаки. Сериализуйте объект человека и сохраните в файл. Десериализуйте
объект человека перейдите по ссылке к собаке и распечатайте строковое представление собаки*/
public class serial {

    public static void main(String[] args) {

        Thing hum = new Thing("Human");
        Thing dog = new Thing("Dog");
        hum.setSpouse(dog);
        dog.setSpouse(hum);

        System.out.println(hum);
        System.out.println(dog);

        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        try {
            fos = new FileOutputStream("I:\\FileIO\\person.dat");
            oos = new ObjectOutputStream(fos);

            oos.writeObject(hum);

        } catch (IOException e) {
            System.out.println("An I/O error occured");
        } finally {
            try {
                if (oos != null)
                    oos.close();
            } catch (IOException e) {
                System.out.println("Error closing file");
            }
        }
    }
}

class Thing implements Serializable {

    public Thing(String name) {
        this.name = name;
        System.out.println("Person constructor is called, name=" + name);
    }

    public Thing() {
        this.name = "A person";
        System.out.println("Person parameterless constructor is called, name="
                + name);
    }

    public String getName() {
        return name;
    }

    public void setSpouse(Thing value) {
        spouse = value;
    }

    public Thing getSpouse() {
        return spouse;
    }

    public String toString() {
        return "[Person: name=" + name + " spouse=" + spouse.getName() + "]";
    }

    private String name;
    private Thing spouse;
}

class TwoObjectsInputDemo {

    public static void main(String[] args) {

        FileInputStream fis = null;
        ObjectInputStream ois = null;

        try {

            fis = new FileInputStream("I:\\FileIO\\person.dat");
            ois = new ObjectInputStream(fis);

            Thing hum = (Thing) ois.readObject();
            System.out.println(hum);
            System.out.println(hum.getSpouse());

        } catch (IOException e) {
            System.out.println("An I/O error occured");
        } catch (ClassNotFoundException e) {
            System.out.println("Class was not found");
        }
        finally {
            try {
                if (ois != null)
                    ois.close();
            } catch (IOException e) {
                System.out.println("Error closing file");
            }
        }
    }
}
