import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;
/* Напишите приложение для проверки что два файла одинаковы. Для этот приложение
выполняет побайтовое чтение файлов и сравнивает байты */
public class bs1 {

    public static void main(String[] args) throws IOException {

        FileInputStream in = null;
        int[] ints = new int[25];
        int temp;
        FileInputStream in1 = null;
        int[] ints1 = new int[25];
        int temp1;


        try {
            in = new FileInputStream("D:\\files\\bytesfile.txt");
            for (int i = 0; i < 25; i++) {
                temp = in.read();
                if (temp == -1)
                    break;

                ints[i] = temp;
            }
            in1 = new FileInputStream("D:\\files\\bytesfile1.txt");
            for (int i = 0; i < 25; i++) {
                temp1 = in1.read();
                if (temp1 == -1)
                    break;

                ints1[i] = temp1;
            }
        } catch (IOException e) {
            System.out.println("An I/O error occured");
        } finally {
            try {
                if (in != null)
                    in.close();
                if (in1 != null)
                    in1.close();
            } catch (IOException e) {
                System.out.println("Error closing file");
            }
        }
        System.out.println("Array one: " + Arrays.toString(ints));
        System.out.println("Array two: " + Arrays.toString(ints1));

        System.out.println("Comparing arrays one and two.....");

        System.out.println("Using equals(): " + ints.equals(ints1));
        System.out.println("Using Arrays.equals(): " + Arrays.equals(ints,ints1));

    }
}
