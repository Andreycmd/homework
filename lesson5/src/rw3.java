import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Arrays;
import java.util.Scanner;
/*Напишите программу которая запрашивает у пользователя имя файла и выводит число
символов слов и строк в этом файле.*/

public class rw3 {

    public static void main(String[] args) throws IOException {

        FileReader in = null;

        try {
            in = new FileReader("D:\\files\\test.txt");
            Scanner sc = new Scanner(new FileReader("D:\\files\\test.txt"));
            LineNumberReader lineNumberReader = new LineNumberReader(in);
            char[] chars = new char[128];
            int nread = in.read(chars);
            int countWord = 0;
            int lineNumber = 0;
            while (sc.hasNextLine()){
                String[] array = sc.nextLine().split(" ");
                        countWord = countWord + array.length;
            }
            while (lineNumberReader.readLine() != null){
                lineNumber++;
            }

            if (nread > 0) {
                System.out.println("Read " + nread + " characters");
                System.out.println("Read " + countWord + " words");
                System.out.println("Read " + lineNumber + " lines");
            }

        } catch (IOException e) {
            System.out.println("An I/O error occured");
        } finally {
            try {
                if (in != null)
                    in.close();
            } catch (IOException e) {
                System.out.println("Error closing file");
            }
        }
    }
}
