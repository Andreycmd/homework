import java.io.*;
import java.nio.charset.Charset;
import java.util.Arrays;
/*Напишите программу которая выполняет следующие действия:
    Открывает файл hello.txt.
    Сохраняет сообдение “Привет, Мир!” в файле.
    Закрывает файл.
    Открывает файл снова.
    Считывает сообщение в строковую переменную и выводит в консоль.*/
public class rw1 {

    public static void main(String[] args) throws IOException {

        String source = "Hello World!";
        FileWriter out = null;

        System.out.println("String to write: " + source );
        System.out.println("Default charset (encoding): " + Charset.defaultCharset());
        System.out.println("String bytes using default encoding: " + Arrays.toString(source.getBytes()));

        try {
            out = new FileWriter("D:\\files\\hello.txt");
            out.write(source);
        } catch (IOException e) {
            System.out.println("An I/O error occured");
        } finally {
            try {
                if (out != null)
                    out.close();
            } catch (IOException e) {
                System.out.println("Error closing file");
            }
        }

            }
        }




