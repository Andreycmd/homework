import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
/* Напишите приложение для разбиения файла размером не более 1000 байт на файлы размером не больше 10 байт.
Названия файлов на которые первоначальный файл разбивается должны заканчиваться
на двузначный порядковый номер файла*/
public class bs2 {
    public static void main(String[] args , String fileName, int partCount) throws Exception{
        Path filePath = Paths.get(fileName);
        try (BufferedInputStream input = new BufferedInputStream(Files.newInputStream(filePath))) {
            long size = Files.size(filePath);
            byte[] buffer = new byte[1024];
            long partSize = size / partCount;
            for (int i = 0; i < partCount; i++){
                String partFileName = fileName + "$" + i;
                try (OutputStream outputStream = Files.newOutputStream(Paths.get(partFileName));
                BufferedOutputStream output = new BufferedOutputStream(outputStream)) {
                    int currentSize = 0;
                    while (currentSize < partSize) {
                        int byteCount = input.read(buffer);
                        output.write(buffer, 0 , byteCount);
                        currentSize += byteCount;
                    }

                }
            }
        }
    }
}

