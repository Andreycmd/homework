/*Напишите приложение для чтения строк с консоли используя Scanner. Закройте Scanner и попробуйте прочитать
        следующую строку. Выполнение должно завершиться исключением IllegalStateException.*/

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class standart1 {

    public static void main(String[] args) {

        Set<String> staff = new TreeSet<String>();

        staff.add("Harry Hacker");
        staff.add("Tonny Tester");
        staff.add("Eve Engineer");
        staff.add("Carl Cracker");

        Iterator<String> iter = staff.iterator();

        System.out.println(iter.next());
        System.out.println(iter.next());
        iter.remove();
        iter.remove();
    }
}
