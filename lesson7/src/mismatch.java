import java.util.Scanner;
/*Разработайте консольное приложение для сложения 2 чисел введенных с командной строки. Для получения чисел
используйте Scanner. Для преобразования строк в int используйте метод nextInt. Чтобы не допустить исключение
InputMismatchException проверяйте что можно считать int из строки с помощью метода hasNextInt.
.*/

public class mismatch {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Enter first number: ");
        while (!input.hasNextInt()) {
            input.next();
            System.out.println("Wrong format ..... \nEnter first number: ");
        }
        int n1 = input.nextInt();

        System.out.println("Enter second number: ");
        while (!input.hasNextInt()) {
            input.next();
            System.out.println("Wrong format ..... \nEnter second number: ");
        }
        int n2 = input.nextInt();

        int result = n1 + n2;
        System.out.println("The result is: " + result);
    }
}
