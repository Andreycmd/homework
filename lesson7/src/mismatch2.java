import java.util.Scanner;
/*Разработайте консольное приложение для сложения 2 чисел введенных с командной строки. Для получения чисел
        используйте Scanner. Для преобразования строк в int используйте метод nextInt. В случае если введенная строка
        не число перехватывайте исключение InputMismatchException и запрашивайте число снова.*/

public class mismatch2 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        while (true) {

            try {
                System.out.println("\nInteger division application.\nEnter first number: ");
                int n1 = input.nextInt();

                System.out.println("Enter second number: ");
                int n2 = input.nextInt();

                int result = n1 + n2;
                System.out.println("The result is: " + result);
            } catch (RuntimeException e) {
                System.out.println("Input must be a number, the second number must be non-zero!");
            }
        }
    }
}
