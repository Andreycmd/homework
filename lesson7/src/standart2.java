/*Напишите приложение которое вызывает рекурсивную функцию. Выполнение должно завершиться StackOverflowError.*/
public class standart2 {

    public static void RecursiveSayHello() {

        System.out.println("Hello World!");
        RecursiveSayHello();
    }

    public static void main(String[] args) {
        RecursiveSayHello();
    }
}
