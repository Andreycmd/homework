/*Напишите приложение которое создает большие объекты и сохраняет ссылки на них в массив.
        Выполнение должно завершиться исключением OutOfMemoryError.*/

public class standart3 {

    public static void main(String[] args) {

        int[][] bigArray = new int[10000][];

        for (int i = 0; i < bigArray.length; i++) {

            bigArray[i] = new int[1024*256];
            System.out.println(i + " MB used");
        }
    }
}
