import java.util.Scanner;

public class task4 {
    public static void main(String[] args) {

        int years = 3;

        double interestRate = 5;

        double balance = 1000;

        for(int y = 0; y < years ; y++) {

            double interest = balance * interestRate / 100;
            balance += interest;
            System.out.println("After year " + (y+1) + " your balance is: " + balance);
        }
    }
}

